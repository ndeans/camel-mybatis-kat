package us.deans.programming.camelbatiskat;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CamelBatisKatApplication {

	public static void main(String[] args) {
		SpringApplication.run(CamelBatisKatApplication.class, args);
	}

}
